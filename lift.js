/*
we want to be able to have a number of lifts and a number of people, one per lift, 1 second per floor
we want to be able to pick a person at random and know how long their journey will take them
*/
function Person(name, floor){
	var self = this;
	self.floor = floor;
	self.name = name;
	self.delivered = false;
	self.timeTravelled = 0;
}
Person.prototype = {
	constructor: Person,
	setDelivered: function(){
		this.delivered = true;
	},
	isDelivered: function(){
		return this.delivered;
	},
	setTimeTravelled: function(time){
		this.timeTravelled = time;
	},
	getTimeTravelled: function(time){
		return this.timeTravelled;
	},
	getFloor: function(){
		return this.floor;
	}
	
}
function Lift(liftQueue, id){
	var self = this;
	self.liftId = id;
	self.returnTime = 0;
	self.totalTravelTime = 0;
	self.liftQueue = liftQueue;
	self.passengersTravelled = [];

}
Lift.prototype = {
	constructor: Lift,
	pickUpPassenger: function(){
		var self = this;
		if(!self.inUse()){
			var person = self.liftQueue.getNextPassenger();
			if(person !== false){
				person.setTimeTravelled(self.totalTravelTime+person.getFloor());
				self.returnTime = person.getFloor()*2;
				self.totalTravelTime += self.returnTime;
				self.passengersTravelled.push(person.name);
				person.setDelivered();
				self.pickUpPassenger();
			}
		} else {
			var lift = self.liftQueue.getFreeLift();
			if(lift !== false){
				lift.pickUpPassenger();			
			} else {
				self.pickUpPassenger();
			}
		}
	},
	inUse: function(){
		var self = this;
		return self.returnTime !== 0;
	},
	moveLiftBackOneFloor: function(){
		var self = this;
		if(self.returnTime !== 0){
			self.returnTime -= 1;
		}
	}
}
function LiftQueue(){
	var self = this;
	self.liftCount = 0;
	self.people = [];
	self.lifts = [];
	
}
LiftQueue.prototype = {
	constructor: LiftQueue,
	addPassenger: function(name, floor){
		var self = this;
		self.people.push(new Person(name, floor));
	},
	addPassengers: function(arr){
		var self = this;
		for(var x = 0, arrLen = arr.length; x < arrLen; x++){
			self.addPassenger(arr[x].name, arr[x].floor);
		}
	},
	setLifts: function(num){
		var self = this;
		for(var x = 0; x < num; x++){
			self.lifts.push(new Lift(self, x));
		}
	},
	getNextPassenger: function(){
		var self = this;
		for(var x = 0, xLen = self.people.length; x < xLen; x++){
			var person = self.people[x];
			if(!person.isDelivered()){
				return person;
			}
		}
		return false;
	},
	getFreeLift: function(){
		var self = this, available = false;
		for(var x = 0, xLen = self.lifts.length; x < xLen; x++){
			var lift = self.lifts[x];
			lift.moveLiftBackOneFloor();
			if(!lift.inUse()){
				available = lift;
			}
		}
		return available;

	},
	run: function(){
		this.lifts[0].pickUpPassenger();
	},
	getTravelTimeForPassenger: function(name){
		var self = this;
		for(var x = 0, xLen = self.people.length; x < xLen; x++){
			if(self.people[x].name === name){
				return self.people[x].timeTravelled;
			}
		}
		return false;
	}
}
var lQ = new LiftQueue();
lQ.setLifts(2);
lQ.addPassenger("A", 2);
lQ.addPassenger("B", 10);
lQ.addPassenger("C", 4);
lQ.addPassenger("D", 6);
lQ.addPassenger("E", 7);
lQ.run();
console.log(lQ, lQ.getTravelTimeForPassenger("E"));